extends Area2D

@export var texture1: Texture2D

@export var texture2: Texture2D

var random = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	random = randi_range(0,1)
	
	if(random == 0):
		$Sprite2D.texture = texture1
	else:
		$Sprite2D.texture = texture2
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_body_entered(body):
	if(body.is_in_group("player")):
		
		body.activatePowerup(random)
		
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
		
	pass # Replace with function body.
