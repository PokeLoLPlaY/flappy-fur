extends CanvasLayer

var volume = 50

var Delay_temp = 1.5
var rango_tuberia = 200

signal returnmenu
# Called when the node enters the scene tree for the first time.
func _ready():
	$VolumeSet.hide()
	$DifficultSet.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_volume_pressed():
	$DifficultSet.hide()
	$VolumeSet.show()
	get_tree().get_root().get_node("Main").find_child("AudioStreamPlayer2D").play()
	rechargeVolumeLabel()


func _on_difficulty_pressed():
	$VolumeSet.hide()
	$DifficultSet.show()
	get_tree().get_root().get_node("Main").find_child("AudioStreamPlayer2D").stop()
	
	
func rechargeVolumeLabel():
	$VolumeSet/VolumeLabel.text = str(volume)
	get_tree().get_root().get_node("Main").find_child("AudioStreamPlayer2D").volume_db = volume-80


func _on_less_pressed():
	if volume>0:
		volume -= 10
		rechargeVolumeLabel()


func _on_plus_pressed():
	if volume<100:
		volume += 10
		rechargeVolumeLabel()


func _on_easy_pressed():
	Delay_temp = 2.5
	rango_tuberia = 150


func _on_normal_pressed():
	Delay_temp = 2
	rango_tuberia = 200


func _on_hard_pressed():
	Delay_temp = 1.5
	rango_tuberia = 225


func _on_return_to_menu_pressed():
	get_tree().get_root().get_node("Main").find_child("AudioStreamPlayer2D").stop()
	returnmenu.emit()
