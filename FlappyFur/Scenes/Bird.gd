extends CharacterBody2D

const gravedad = 1000
const Max_Vel = 600
const Vel_Aleteo = -500

const start_pos = Vector2(100,400)

var vuela = false
var cae = false

enum states {
	normal, invinciblePower, slowPower
}

var currentState: states = states.normal

var powerupTimerMax = 10

signal slow

signal fast

var timerPocho = 0

func startanimation():
	$AnimatedSprite2D.play()

# Get the gravity from the project settings to be synced with RigidBody nodes.
func _ready():
	$powerupTimer.wait_time = powerupTimerMax
	reset()

func _process(delta):
	if(currentState != states.normal):
		parpadear()
	pass	
	
func reset():
	cae = false
	vuela = false
	position = start_pos
	set_rotation(0)
	changeState(states.normal)
	$AnimatedSprite2D.stop()

func _physics_process(delta):
	# Add the gravity.
	if cae or vuela:
		velocity.y += gravedad * delta
		
		if velocity.y > Max_Vel:
			velocity.y = Max_Vel
		
		if vuela:
			set_rotation(deg_to_rad(velocity.y * 0.05))
		elif cae:
			set_rotation(PI/2)
			$AnimatedSprite2D.stop()
		
		move_and_collide(velocity * delta)
	else:
		$AnimatedSprite2D.stop()

func aleteo():
	velocity.y = Vel_Aleteo
	
func activatePowerup(number: int):
	if(number == 0):
		changeState(states.invinciblePower)
	elif(number == 1):
		changeState(states.slowPower)

func parpadear():
	
	timerPocho +=1
	
	if(int($powerupTimer.time_left) < 3):
		if(timerPocho % 10 == 0):
			var tween: Tween = create_tween()
			tween.tween_property($AnimatedSprite2D, "modulate:v", 1, 0.1).from(15)
	else:
		if(int($powerupTimer.time_left) % 2 == 0):
			var tween: Tween = create_tween()
			tween.tween_property($AnimatedSprite2D, "modulate:v", 1, 0.1).from(15)
	
func changeState(newState: states):
	currentState = newState
	
	match currentState:
		states.normal:
			fast.emit()
			pass
		states.invinciblePower:
			$powerupTimer.start()
			fast.emit()
			#parpadear()
			pass
		states.slowPower:
			$powerupTimer.start()
			#parpadear()
			slow.emit()
			pass
	pass


func _on_powerup_timer_timeout():
	$powerupTimer.stop()
	$powerupTimer.wait_time = powerupTimerMax
	changeState(states.normal)
	pass # Replace with function body.
