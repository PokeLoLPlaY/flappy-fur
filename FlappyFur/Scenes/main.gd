extends Node

@export var Scene_Tuberia : PackedScene

var scroll
var score
var screensize : Vector2i
var ground_height : int

var Tuberias : Array

var on_game : bool
var game_over : bool
var onMenu : bool

const Delay_tuberia = 150

var scroll_speed = 300


# Called when the node enters the scene tree for the first time.
func _ready():
	
	screensize = get_window().size
	ground_height = $Suelo.get_node("Sprite2D").texture.get_height()
	MainMenu()
	$Timer_Tuberias.wait_time = $Configuration_Menu.Delay_temp
	
func MainMenu():
	$Main_Menu.show()
	$Game_Over.hide()
	$Configuration_Menu.hide()
	$Score_Label.hide()
	onMenu=true
	

func newGame():
	$Score_Label.show()
	onMenu = false
	on_game = false
	game_over = false
	
	score = 0
	scroll = 0
	
	$Score_Label.text = "SCORE: 0"
	
	get_tree().call_group("Tuberias", "queue_free")
	Tuberias.clear()
	nueva_tuberia()
	$Bird.reset()
	$Game_Over.hide()
	$Main_Menu.hide()
	

func _input(event):
	if game_over == false and !onMenu:
		if event is InputEventMouseButton:
			if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
				if on_game == false:
					start_game()
				else: 
					if $Bird.vuela:
						$Bird.aleteo()
						check_top()

func start_game():
	on_game = true
	$Bird.vuela=true
	$Bird.aleteo()
	$Timer_Tuberias.wait_time = $Configuration_Menu.Delay_temp
	$Timer_Tuberias.start()
	$AudioStreamPlayer2D.play()
	$Bird.startanimation()

func showconfigurations():
	$Configuration_Menu.show()
	$Main_Menu.hide()

func hideconfigurations():
	$Configuration_Menu.hide()
	$Main_Menu.show()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if on_game:
		scroll += scroll_speed*delta
			
		if scroll >= screensize.x:
			scroll = 0
			
		$Suelo.position.x = -scroll
		
		for tuberia in Tuberias:
			tuberia.position.x -= scroll_speed*delta


func _on_timer_tuberias_timeout():
	nueva_tuberia()

func nueva_tuberia():
	var tuberia = Scene_Tuberia.instantiate()
	
	tuberia.position.x = screensize.x + Delay_tuberia
	tuberia.position.y = (screensize.y - ground_height)/2 + randi_range(-$Configuration_Menu.rango_tuberia, $Configuration_Menu.rango_tuberia)
	tuberia.hit.connect(bird_hit)
	tuberia.score.connect(scored)
	
	add_child(tuberia)
	Tuberias.append(tuberia)
	
func scored():
	score+=1
	$Score_Label.text = "SCORE: "+ str(score)


func check_top():
	if $Bird.position.y < 0:
		$Bird.cae = true
		stop_game()
		
func stop_game():
		$Timer_Tuberias.stop()
		$Bird.vuela = false
		on_game = false
		game_over = true
		$Game_Over.show()
		$AudioStreamPlayer2D.stop()

func bird_hit():
	if !onMenu:
		$Bird.cae = true
		stop_game()



func _on_suelo_hit():
	if !onMenu:
		$Bird.cae = true
		stop_game()


func _on_game_over_restart():
	newGame()



func _on_main_menu_start_game():
	newGame()



func _on_game_over_mainmenu():
	MainMenu()


func _on_main_menu_configuration():
	showconfigurations()


func _on_configuration_menu_returnmenu():
	hideconfigurations()





func _on_bird_fast():
	scroll_speed = 300
	$Timer_Tuberias.wait_time = $Configuration_Menu.Delay_temp
	pass # Replace with function body.


func _on_bird_slow():
	scroll_speed = 200
	
	$Timer_Tuberias.wait_time = $Configuration_Menu.Delay_temp*2
	pass # Replace with function body.
