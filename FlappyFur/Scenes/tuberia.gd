extends Area2D


signal hit
signal score

var falling = false
var fallspd =250;

func _ready():
	var random = randi_range(0,10)
	
	if(random != 0):
		$powerup.hide()
		$powerup/CollisionShape2D.set_deferred("disabled", true)
	

func _process(delta):
	if(falling == true):
		fall(delta)

func _on_body_entered(body):
	if(body.currentState == body.states.invinciblePower):
		falling = true
	else:
		hit.emit()

func _on_score_body_entered(body):
	score.emit()

func fall(delta : float):
	#set_collision_layer_value(1, false)
	self.position.y = self.position.y + fallspd*delta
	self.rotate(-0.5*delta)
