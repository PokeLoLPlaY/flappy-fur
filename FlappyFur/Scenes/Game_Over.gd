extends CanvasLayer


signal restart
signal mainmenu

func _on_restart_button_pressed():
	restart.emit()


func _on_salir_al_menu_pressed():
	mainmenu.emit()
