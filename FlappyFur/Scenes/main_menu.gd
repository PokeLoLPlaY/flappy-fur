extends CanvasLayer

signal startGame
signal Configuration



func _on_start_game_pressed():
	startGame.emit()


func _on_configuration_pressed():
	Configuration.emit()


func _on_exitgame_pressed():
	get_tree().quit()
